
import './App.css';
import {BrowserRouter ,Route,Link,Routes} from 'react-router-dom';
import UserComponent from './UserComponent';
import UserEmail from './UserEmail';
function App() {
  return (
    <div className="App">
      <h2>HOME</h2>
      <BrowserRouter>
      <ul>
      <li><Link to="/Comp1">USERLIST</Link></li>
      <li><Link to="/email">Email</Link></li>
      </ul>
      <Routes>
      <Route path="/Comp1" element={<UserComponent />}>
      </Route>
      <Route path="/email" element={<UserEmail/>}>
      </Route>
      </Routes>
    </BrowserRouter>
  </div>

  );
}

export default App;
